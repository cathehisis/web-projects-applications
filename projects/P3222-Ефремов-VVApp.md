# Площадка для поиска и проведения кампаний НРИ в системе VV "VVApp"
## Автор
P3222 Ефремов Андрей Игоревич
## Описание
Данная площадка позволит любителям настольных ролевых игр системы VV искать себе единомышленников и создавать партии, формировать кампании. Пользователи, авторизованные как мастера могут создавать игры и приглашать туда игроков, пользователи, авторизованные как игроки, могуть подавать запросы на вступление в партии. Также приложение содержит систему управления электронными листами персонажей для упрощения игрового процесса. 
## Детали 
- Продукт - веб-приложение c базой данных, пользовательский интерфейс - веб-страницы.
- Стек технологий - SpringMVC, MySQL, Thymeleaf.